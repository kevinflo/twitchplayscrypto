This was a live streamed crowdsourced cryptocurrency portfolio at [https://www.twitch.tv/twitchplayscrypto](https://www.twitch.tv/twitchplayscrypto). Viewers could see the balances of different cryptocurrencies in the portfolio and execute !buy and !sell orders in chat, which would make real trades in real time

It was live for about 6 months, but I took it down as I couldn't run it while doing some extended travel

Overall the stream got almost 20,000 views. I didn't get rich off it or anything, but after taxes I did end up a bit profitable

I wrote the code for this in like a day and a half. This was a for-fun side project and the code is not meant to be representative of what I consider to be quality code.